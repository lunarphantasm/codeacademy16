#include "Note.h"
#include <chrono>

inline uint64_t current_timestamp()
{
    return std::chrono::seconds(std::time(NULL)).count();
}

Note::Note()
    : m_name()
    , m_text()
    , m_creation_timestamp(current_timestamp())
{}

Note::Note(const std::string& name, const std::string& text)
    : m_name(name)
    , m_text(text)
    , m_creation_timestamp(current_timestamp())
{}

std::string Note::name() const
{
    return m_name;
}

void Note::name(const std::string& name)
{
    if (!name.empty())
    {
        m_name = name;
    }
}

std::string Note::text() const
{
    return m_text;
}

void Note::text(const std::string& text)
{
    m_text = text;
}

std::string Note::creation_time() const
{
/* TODO: Refactor this to use more modern time format functions, maybe, put_time from iomanip */
    auto t = std::time_t(m_creation_timestamp);
    auto ctm = std::string(std::ctime(&t));
    return ctm.substr(0, ctm.length()-1);
}
