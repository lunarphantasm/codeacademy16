#ifndef CONTACTBOOK_H
#define CONTACTBOOK_H

#include "GenericBook.hpp"
#include "Contact.h"


class ContactBook : public GenericBook<Contact>
{
public:
    bool add(const std::string& name, const std::string& phone = "", const std::string& email = "");
};

#endif // CONTACTBOOK_H
