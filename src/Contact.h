#ifndef CONTACT_H
#define CONTACT_H

#include <string>


class Contact
{
public:
    Contact();
    Contact(const std::string& name, const std::string& phone = "", const std::string& email = "");

    std::string name() const;
    void name(const std::string& name);

    std::string phone() const;
    void phone(const std::string& phone);

    std::string email() const;
    void email(const std::string& email);

public:
    // cereal serialization
    template <class Arc>
    void serialize(Arc& archive)
    {
        archive(m_name, m_phone, m_email);
    }

private:
    std::string m_name;
    std::string m_phone;
    std::string m_email;
};

#endif // CONTACT_H
