#ifndef TODO_H
#define TODO_H

#include <string>


class Todo
{
public:
    Todo();
    Todo(const std::string& name, const std::string& text = "", const size_t priority = 0, const bool completed = false);

    std::string name() const;
    void name(const std::string& name);

    std::string text() const;
    void text(const std::string& text);

    size_t priority() const;
    void priority(const size_t priority);

    bool completed() const;
    void completed(const bool completed);

public:
    // cereal serialization
    template <class Arc>
    void serialize(Arc& archive)
    {
        archive(m_name, m_text, m_priority, m_completed);
    }

private:
    std::string m_name;
    std::string m_text;
    size_t m_priority;
    bool m_completed;
};

#endif //
