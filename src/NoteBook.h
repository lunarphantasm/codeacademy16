#ifndef NOTEBOOK_H
#define NOTEBOOK_H

#include "GenericBook.hpp"
#include "Note.h"

class NoteBook : public GenericBook<Note>
{
public:
    bool add(const std::string& name, const std::string& text = "");
};

#endif // NOTEBOOK_H
