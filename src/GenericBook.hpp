#ifndef GENERICBOOK_H
#define GENERICBOOK_H

#include <string>
#include <vector>
#include <unordered_map>

#include <algorithm>
#include <type_traits>


template <class T>
class GenericBook
{
public:
    GenericBook();
    virtual ~GenericBook();

    bool add(const std::string& name, const T& what);
    bool del(const std::string& name);

    std::vector<std::string> list() const;
    std::vector<std::string> search(const std::string& substring);

    template <class FN>
    typename std::enable_if<std::is_function<FN(const T&)>::value, std::vector<std::string>>::type
    filter(FN predicate);

    size_t size() const;
    T get(const std::string& name) const;

    void clear();

    template <class Arc>
    void serialize(Arc& archive) { archive(m_records); }

protected:
    std::unordered_map<std::string, T> m_records;
};

// --- implementation ---
template <class T>
GenericBook<T>::GenericBook()
    : m_records()
{}

template <class T>
GenericBook<T>::~GenericBook()
{}

template <class T>
bool GenericBook<T>::add(const std::string& name, const T& what)
{
    bool result = false;
    if (m_records.find(name) == m_records.end())
    {
        m_records[name] = what;
        result = true;
    }
    return result;
}

template <class T>
bool GenericBook<T>::del(const std::string& name)
{
    bool result = false;
    auto it = m_records.find(name);
    if (it != m_records.end())
    {
        m_records.erase(it);
    }
    return result;
}

template <class T>
size_t GenericBook<T>::size() const
{
    return m_records.size();
}

template <class T>
std::vector<std::string> GenericBook<T>::list() const
{
    auto names = std::vector<std::string>();
    names.reserve(m_records.size());
    for (const auto entry : m_records)
    {
        names.push_back(entry.first);
    }
    return names;
}

template <class T>
std::vector<std::string> GenericBook<T>::search(const std::string& substring)
{
    auto names = std::vector<std::string>();
    for (auto& entry : m_records)
    {
        if (entry.first.find(substring) != std::string::npos)
        {
            names.push_back(entry.first);
        }
    }
    return names;
}

template <class T>
template <class FN>
typename std::enable_if<std::is_function<FN(const T&)>::value, std::vector<std::string>>::type
GenericBook<T>::filter(FN predicate)
{
    auto names = std::vector<std::string>();
    std::for_each(std::begin(m_records), std::end(m_records),
                  [&](auto& entry) -> auto
                  {
                      if (predicate(entry.second)) names.push_back(entry.first);
                  });
    return names;
}

template <class T>
T GenericBook<T>::get(const std::string& name) const
{
    auto it = m_records.find(name);
    return ((it != m_records.end()) ? m_records.at(name) : T());
}

template <class T>
void GenericBook<T>::clear()
{
    m_records.clear();
}

#endif // GENERICBOOK_H
