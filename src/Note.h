#ifndef NOTE_H
#define NOTE_H

#include <string>


class Note
{
public:
    Note();
    Note(const std::string& name, const std::string& text = "");

    std::string name() const;
    void name(const std::string& name);

    std::string text() const;
    void text(const std::string& text);

    std::string creation_time() const;

public:
    // cereal serialization
    template <class Arc>
    void serialize(Arc& archive)
    {
        archive(m_name, m_text, m_creation_timestamp);
    }

private:
    std::string m_name;
    std::string m_text;
    uint64_t m_creation_timestamp;
};

#endif // NOTE_H
