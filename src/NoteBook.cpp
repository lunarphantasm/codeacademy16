#include "NoteBook.h"


bool NoteBook::add(const std::string& name, const std::string& text)
{
    const auto note = Note(name, text);
    return GenericBook::add(name, note);
}
