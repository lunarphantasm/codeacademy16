#include "ContactBook.h"


bool ContactBook::add(const std::string& name, const std::string& phone, const std::string& email)
{
    const auto contact = Contact(name, phone, email);
    return GenericBook::add(name, contact);
}
