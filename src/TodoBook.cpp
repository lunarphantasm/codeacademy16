#include "TodoBook.h"


bool TodoBook::add(const std::string& name, const std::string& text, const size_t priority, const bool done)
{
    const auto todo = Todo(name, text, priority, done);
    return GenericBook::add(name, todo);
}
