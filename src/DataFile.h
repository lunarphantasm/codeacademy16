#ifndef DATAFILE_H
#define DATAFILE_H

#include <string>


class DataFile
{
public:
    static std::string name();
    static std::string notesSection();
    static std::string todosSection();
    static std::string contactsSection();

private:
    static std::string join(const auto& left, const auto& right);
};

#endif // DATAFILE_H
