#ifndef ORGANIZER_H
#define ORGANIZER_H

#include "DataFile.h"
#include "Archiver.hpp"

#include "NoteBook.h"
#include "TodoBook.h"
#include "ContactBook.h"

class Organizer
{
public:
    Organizer(const std::string& file = DataFile::name());
    ~Organizer();

    NoteBook& notes() { return m_notes; }
    TodoBook& todos() { return m_todos; }
    ContactBook& contacts() { return m_contacts; }

private:
    bool load();
    bool save();

    template <ar::io::Operation OP> bool serialize();

private:
    NoteBook m_notes;
    TodoBook m_todos;
    ContactBook m_contacts;

    const std::string m_file;
};

#endif // ORGANIZER_H
