#ifndef TODOBOOK_H
#define TODOBOOK_H

#include "GenericBook.hpp"
#include "Todo.h"

class TodoBook : public GenericBook<Todo>
{
public:
    bool add(const std::string& name, const std::string& text = "", const size_t priority = 0, const bool done = false);
};

#endif //
