#include "Todo.h"


Todo::Todo()
    : m_name()
    , m_text()
    , m_priority(0)
    , m_completed(false)
{}

Todo::Todo(const std::string& name, const std::string& text, const size_t priority, const bool completed)
    : m_name(name)
    , m_text(text)
    , m_priority(priority)
    , m_completed(completed)
{}

std::string Todo::name() const
{
    return m_name;
}

void Todo::name(const std::string& name)
{
    if (!name.empty())
    {
        m_name = name;
    }
}

std::string Todo::text() const
{
    return m_text;
}

void Todo::text(const std::string& text)
{
    m_text = text;
}

size_t Todo::priority() const
{
    return m_priority;
}

void Todo::priority(const size_t priority)
{
    m_priority = priority;
}

bool Todo::completed() const
{
    return m_completed;
}

void Todo::completed(const bool completed)
{
    m_completed = completed;
}
