#include "Organizer.h"

#include <cereal/types/unordered_map.hpp>
#include <cereal/types/string.hpp>

#include <type_traits>
#include <stdexcept>
#include <fstream>


Organizer::Organizer(const std::string& file)
    : m_notes()
    , m_contacts()
    , m_file(file)
{
    load();
}

Organizer::~Organizer()
{
    save();
}

template <ar::io::Operation OP>
bool Organizer::serialize()
{
    using namespace ar;

    constexpr auto fmt = format::JSON;

    typedef typename std::conditional<OP == io::READ, std::ifstream, std::ofstream>::type StreamType;
    typedef typename std::conditional<OP == io::READ, Archiver<fmt, io::INPUT>::type, Archiver<fmt, io::OUTPUT>::type>::type ArchiveType;

    try
    {
        StreamType fs(m_file);
        ArchiveType archive(fs);

        archive(cereal::make_nvp(DataFile::notesSection(), notes()));
        archive(cereal::make_nvp(DataFile::todosSection(), todos()));
        archive(cereal::make_nvp(DataFile::contactsSection(), contacts()));
    }
    catch (std::exception& e)
    {
        return false;
    }
    return true;
}

bool Organizer::load()
{
    return serialize<ar::io::READ>();
}

bool Organizer::save()
{
    return serialize<ar::io::WRITE>();
}
