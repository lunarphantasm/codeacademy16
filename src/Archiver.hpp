#ifndef ARCHIVEPROVIDER_H
#define ARCHIVEPROVIDER_H

#include <cereal/archives/binary.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/archives/xml.hpp>

namespace ar {

namespace io
{
    enum Operation          { READ, WRITE };
    enum StreamDirection    { UNSPECIFIED = 0, INPUT, OUTPUT };
}

namespace format
{
    enum DataFormat         { UNSPECIFIED = 0, BINARY, JSON, XML };
}

template <ar::format::DataFormat F = ar::format::UNSPECIFIED, ar::io::StreamDirection D = ar::io::UNSPECIFIED>
struct Archiver {};

template <> struct Archiver<ar::format::JSON, ar::io::INPUT> { typedef cereal::JSONInputArchive type; };
template <> struct Archiver<ar::format::JSON, ar::io::OUTPUT> { typedef cereal::JSONOutputArchive type; };

template <> struct Archiver<ar::format::XML, ar::io::INPUT> { typedef cereal::XMLInputArchive type; };
template <> struct Archiver<ar::format::XML, ar::io::OUTPUT> { typedef cereal::XMLOutputArchive type; };

template <> struct Archiver<ar::format::BINARY, ar::io::INPUT> { typedef cereal::BinaryInputArchive type; };
template <> struct Archiver<ar::format::BINARY, ar::io::OUTPUT> { typedef cereal::BinaryOutputArchive type; };

} // namespace ar

#endif // ARCHIVEPROVIDER_H
