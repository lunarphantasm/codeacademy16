#include "DataFile.h"


static constexpr auto defaultBase           = "ac16org";
static constexpr auto defaultExtension      = "dat";
static constexpr auto defaultNotesToken     = "notes";
static constexpr auto defaultTodosToken     = "todos";
static constexpr auto defaultContactsToken  = "contacts";


std::string DataFile::name()
{
    return join(defaultBase, defaultExtension);
}

std::string DataFile::notesSection()
{
    return join(defaultBase, defaultNotesToken);
}

std::string DataFile::todosSection()
{
    return join(defaultBase, defaultTodosToken);
}

std::string DataFile::contactsSection()
{
    return join(defaultBase, defaultContactsToken);
}

std::string DataFile::join(const auto& left, const auto& right)
{
    return std::string(left) + std::string(".") + std::string(right);
}
