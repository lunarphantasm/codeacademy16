#include "Contact.h"

Contact::Contact()
    : m_name()
    , m_phone()
    , m_email()
{}

Contact::Contact(const std::string& name, const std::string& phone, const std::string& email)
    : m_name(name)
    , m_phone(phone)
    , m_email(email)
{}

std::string Contact::name() const
{
    return m_name;
}

void Contact::name(const std::string& name)
{
    if (!name.empty())
    {
        m_name = name;
    }
}

std::string Contact::phone() const
{
    return m_phone;
}

void Contact::phone(const std::string& phone)
{
    m_phone = phone;
}

std::string Contact::email() const
{
    return m_email;
}

void Contact::email(const std::string& email)
{
    m_email = email;
}
