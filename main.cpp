#include <iostream>

#include "Organizer.h"


template <class T> void print(const T& what);

template <> void print<Note>(const Note& what)
{
    std::cout
    << "name: " << what.name() << " \t "
    << "text: " << what.text() << " \t "
    << "date: " << what.creation_time()
    << std::endl;
}

template <> void print<Contact>(const Contact& what)
{
    std::cout
    << "name: " << what.name() << " \t "
    << "phone: " << what.phone() << " \t "
    << "email: " << what.email()
    << std::endl;
}

template <> void print<Todo>(const Todo& what)
{
    std::cout
    << "name: " << what.name() << " \t "
    << "text: " << what.text() << " \t "
    << "priority: " << what.priority() << " \t "
    << "complete: " << std::boolalpha << what.completed()
    << std::endl;
}

template <> void print<std::string>(const std::string& what)
{
    std::cout << what << std::endl;
}

template <class T>
void print(const std::vector<T>& vec)
{
    for (const auto& entry : vec)
    {
        print(entry);
    }
}

template <class S, class T>
void print(const std::vector<T>& vec, S source)
{
    for (const auto& entry : vec)
    {
        print(source.get(entry));
    }
}

int main()
{
    auto ac16org = Organizer();

//    for (auto i = 1; i < 19; ++i) ac16org.notes().add(std::to_string(i), std::to_string(i<<1));
//    for (auto i = 1; i < 19; ++i) ac16org.contacts().add(std::to_string(i), std::to_string(i>>1));
//    for (auto i = 1; i < 19; ++i) ac16org.todos().add(std::to_string(i), std::to_string(i<<2), i, i%2);

    {
        auto contain_4 = ac16org.notes().search("4");
        print(contain_4);
        contain_4 = ac16org.contacts().search("4");
        print(contain_4);
        std::cout << "---" << std::endl;
    }
    {
        auto cont_4 = ac16org.contacts().get("4");
        print(cont_4);
        std::cout << "---" << std::endl;
    }
    {
        std::cout << "List notes with name shorter than 2 chars" << std::endl;
        auto filtered = ac16org.notes().filter([](const auto& n){ return (n.name().length() < 2); });
        print(filtered, ac16org.notes());
        std::cout << "---" << std::endl;
    }
    {
        std::cout << "List notes items with name other than '10'" << std::endl;
        auto filtered = ac16org.notes().filter([](const auto& n){ return (n.name() != "10"); });
        print(filtered, ac16org.notes());
        std::cout << "---" << std::endl;
    }
    {
        std::cout << "Print random note with creation time" << std::endl;
        print(ac16org.notes().get("4"));
        std::cout << "---" << std::endl;
    }
    {
        std::cout << "List ToDo items with status NOT DONE" << std::endl;
        auto items = ac16org.todos().filter([](const auto& x){ return !x.completed(); });
        print(items, ac16org.todos());
        std::cout << "---" << std::endl;
    }
    {
        std::cout << "List completed ToDo items sorted by name" << std::endl;
        auto items = ac16org.todos().filter([](const auto& x){ return x.completed(); });
        std::sort(items.begin(), items.end());
        print(items, ac16org.todos());
        std::cout << "---" << std::endl;
    }

    return 0;
}
